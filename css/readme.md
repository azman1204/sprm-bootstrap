# nota asas CSS

## ada 3 cara guna css
1. <style>...</style>
2. inline styling i.e <div style="...">
3. dlm external file .css di include <link href='..css' rel='stylesheet'>

# vsc shortcut
- ctl + b = toggle explorer
- shift + alt + arrow down = copy ke bawah
- f2 = rename file
- ctl + / = comment
- shift + tab = indent ke belakang (opposite of tab)
- ctl + enter = go next line

# 10 css selector rules
1. by tag name
   - h1 { color: blue }
2. by class name .
3. by id #
4. space (inside)
5. , (AND)
6. > direct parent
7. sibling (+)
8. semua (*)
9. by attribute name ([])
10. event (:)
    - hover
    - first
    - last
    - after
    - active (link yg di klik)